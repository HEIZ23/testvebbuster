Для проверки кода следует использовать:
1. В PHP конструкцию var_dump() + exit.
2. В JS конструкцию Console.log()/alert().
Что бы отследить движение данных в коде, нужно снизу вверх(как выполняется код) вводить нужную конструкцию.
В случае неожидаемого результата нужно вернуться на шаг назад и посмотреть что происходит с данными до изменения.
