<?php

class Categories
{
    public function connectDb($host, $user, $pass, $nameDB){
        $conn = new mysqli($host, $user, $pass, $nameDB);

        if($conn->connect_error) {
            die("Ошибка:" . $conn->connect_error);
        }

        return $conn;
    }

    public function selectData($db){
        return $db->query("SELECT * FROM categories");
    }

    public function getCategory(){
        $db = $this->connectDb(HOST,USER, PASS, DB);
        $data = $this->selectData($db);

        if(!$data){
            return ('Данных нет');
        }
        $arrCat = [];

        if($data->num_rows != 0){
            for($i = 0; $i < $data->num_rows; $i++){
                $row = $data->fetch_assoc();
                if(empty($arrCat[$row['parent_id']])){
                    $arrCat[$row['parent_id']] = [];
                }
                $arrCat[$row['parent_id']][] = $row;
            }
        }

        return $arrCat;
    }

    public function showCategory($arr, $parent_id = 0){
        if(empty($arr[$parent_id])){
            return;
        }
        echo '<ul>';
        for($i = 0; $i < count($arr[$parent_id]); $i++){
            echo '<li id="row"><a class="row" href="?category_id='.$arr[$parent_id][$i]['id'].
                '&parent_id='. $parent_id . '">'
                .$arr[$parent_id][$i]['title'].'<button class="hide">Х</button>'.'</a>';
            $this->showCategory($arr, $arr[$parent_id][$i]['id']);
            echo'</li>';
        }
        echo '</ul>';
    }
}